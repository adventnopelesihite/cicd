const express = require("express"),
  app = express(),
  bodyParser = require("body-parser"),
  cors = require("cors");

app.use(cors());

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(bodyParser.json());

// ENV
const dotenv = require("dotenv");

dotenv.config({ path: "./api/config/config.env" });

// Connect MongoDB
const connectDB = require("./api/config/connect");
connectDB();

// Routes
app.use("/v1/user", require("./api/routes/users"));

app.use((req, res, next) => {
  const error = new Error("Page Not Found");
  res.status(404);
  next(error);
});

app.use((error, req, res, next) => {
  res.status(404);
  res.json({
    code: 404,
    messsage: error.message,
  });
});

// PORT
const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Server start on port ${port}`));

module.exports = app;