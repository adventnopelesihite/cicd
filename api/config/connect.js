const mongoose = require("mongoose");

const connectDB = async () => {
  try {
    const conn = await mongoose.connect(`${process.env.USER_MONGODB}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      autoIndex: false,
    });

    console.log(`MongoDB Connected : ${conn.connection.host}`);
  } catch (error) {
    process.exit(1);
  }
};

module.exports = connectDB;
