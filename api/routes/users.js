const express = require("express"),
  router = express.Router();
const User = require("../controllers/users");

router.post("/login", User.login);

module.exports = router;
