const User = require("../models/users"),
  jwt = require("jsonwebtoken");

const bcrypt = require("bcrypt");

exports.login = async (req, res) => {
  try {
    const user = await User.findOne({ username: req.body.username }).exec();

    if (user === null) {
      return res.status(404).json({
        code: 404,
        message: "Cannot Found User",
      });
    }

    bcrypt.compare(req.body.password, user.password, async (err, result) => {
      if (err) {
        return res.status(400).json({ code: 400, message: "Bad Request!!" });
      }

      if (result) {
        const token = await jwt.sign(
          {
            id: user.id,
            username: user.username,
            name: user.name,
            email: user.email,
          },
          process.env.JWT_KEY,
          {
            expiresIn: "2 days",
          }
        );

        return res.status(200).json({
          code: 200,
          data: {
            username: user.username,
            token: token,
          },
          message: "Auth Sucessful",
        });
      }

      return res.status(404).json({
        code: 404,
        message: "Invalid Credential.",
      });
    });
  } catch (error) {
    console.log(error);
  }
};
