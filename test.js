const chai = require('chai');
const chaiHttp = require('chai-http');
const User = require('./api/controllers/users');
const server = require('./app');
const should = chai.should();

chai.use(chaiHttp);

describe('/GET login', () => {
    it('Login success', (done) => {
        let requestBody = {
            username: "advent",
            password: "12345678"
        }
        chai.request(server)
            .post('/v1/user/login')
            .send(requestBody)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                done();
            });
    });

    it('Invalid Crendential', (done) => {
        let requestBody = {
            username: "advent",
            password: "12"
        }
        chai.request(server)
            .post('/v1/user/login')
            .send(requestBody)
            .end((err, res) => {
                res.should.have.status(404);
                res.body.should.be.a('object');
                done();
            });
    });

    it('User Not Found', (done) => {
        let requestBody = {
            username: "test",
            password: "1"
        }
        chai.request(server)
            .post('/v1/user/login')
            .send(requestBody)
            .end((err, res) => {
                res.should.have.status(404);
                res.body.should.be.a('object');
                done();
            });
    });

    it('Page Not Found', (done) => {
        chai.request(server)
            .get('/')
            .end((err, res) => {
                res.should.have.status(404);
                res.body.should.be.a('object');
                done();
            });
    });

    it('Bad request', (done) => {
        let requestBody = {
            username: "advent",
            password: 12345678
        }
        chai.request(server)
            .post('/v1/user/login')
            .send(requestBody)
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                done();
            });
    });
});
